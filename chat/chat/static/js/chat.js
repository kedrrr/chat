var app = angular.module("app", [
        'ngUpload',
        'ui.bootstrap.tpls',
        'ui.bootstrap.modal',
        'ui.bootstrap.buttons'
    ]);

app.config(['$provide', '$interpolateProvider', function($provide, $interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
}]);

app.run(['$http', function($http) {
  $http.defaults.headers.post['X-CSRFToken'] = CSRF_TOKEN;
}]);

app.controller("AuthController", function($http, $scope, $uibModal){

    $scope.authModalOpen = function (item) {
        var modalInstance = $uibModal.open({
          templateUrl: "/static/template/auth-modal.html",
          controller: 'authModalOpenCtrl',
          size: 'xs'
        });
      };
    
});

app.controller('authModalOpenCtrl', function ($scope, $modalInstance, $http, $timeout) {

    $scope.item = {
        formType: 'old'
    }
    $scope.alert = "";

    $scope.ok = function () {
        $scope.Form.$setSubmitted();
        if($scope.Form.$valid){
            $http.post("/s/auth.html", $scope.item).
                success(function(data, status, headers, config) {
                    if(data.status == "ok"){
                        document.location.reload();
                    }else{
                        $scope.alert = data.message
                        $timeout(function(){
                            $scope.alert = "";
                        }, 3000);
                    }
                })
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

})



app.controller("MessageController", function($http, $scope){

    $scope.Logout = function(){
        $http.post("/s/logout.html").
                success(function(data, status, headers, config) {
                    document.location.reload();
                })
    }

    $scope.complete = function(content) {
        if(content.value){
            $scope.item.photo = content.value;
            $scope.item.photo_thumbnail = content.url;
            $scope.item.photo_thumbnail_big = content.url_big;
        }
       document.getElementById("message").value = "";
       document.getElementById("file").value = "";
    }
    
});



app.controller("ChatController", function($http, $scope, $timeout){
    $scope.messages = []

    $scope.load = function(){
        $http.post("/s/loadMessages.html", angular.copy($scope.item)).
                then(function(data, status, headers, config) {
                    $scope.messages = data.data.messages;
                    $timeout($scope.load, 1000);
                }, function(data, status, headers, config) {
                    $timeout($scope.load, 1000);
                })
    }

    $scope.load();
    
});