from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from .view import Index
from chat import s

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', Index.as_view(), name="index"),
    
    url(r'^s/auth\.html$', s.Auth, name="s__auth"),
    url(r'^s/logout\.html$', s.Logout, name="s__logout"),

    url(r'^s/addMessage\.html$', s.addMessage, name="s__add_message"),
    url(r'^s/loadMessages\.html$', s.loadMessages, name="s__load_messages"),
]
