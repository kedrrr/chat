# -*- coding: utf-8 -*-
from django.http import HttpResponseForbidden

def auth_only(function=None):

    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if request.user.is_authenticated():
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponseForbidden()

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)