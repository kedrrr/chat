# -*- coding: utf-8 -*-
from django.conf import settings
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.http import require_POST
from django.contrib.auth import authenticate, login, logout
from django.core.files.storage import default_storage
from django.utils.encoding import force_str, force_text
from .decorators import auth_only
from apps.message.models import Message
import json
import os
import datetime

@require_POST
def Auth(request, *args, **kwargs):
    context = {
        'status': 'error',
        'message': ''
    }

    post = json.loads(request.body)
    if post.get('formType') == 'new':
        form = UserCreationForm(post)
        if form.is_valid():
            form.save()
            user = authenticate(username=post.get("username"), password=post.get("password1"))
            login(request, user)
            context['status'] = 'ok'
        else:
            context['message'] = str(form.errors.items()[0][1].as_data()[0][0])

    else:
        context['message'] = "Логин или пароль указан не верно"
        if post.get("username") and post.get("password1"):
            user = authenticate(username=post.get("username"), password=post.get("password1"))
            if user and user.is_active:
                login(request, user)
                context['status'] = 'ok'

    if context['status'] == "ok":
        context['message'] = ""

    return HttpResponse(json.dumps(context), content_type='application/json')


@require_POST
def Logout(request, *args, **kwargs):
    context = {
        'status': 'ok',
        'message': ''
    }

    logout(request)

    return HttpResponse(json.dumps(context), content_type='application/json')


@require_POST
@auth_only
def addMessage(request, *args, **kwargs):
    context = {
        'status': 'ok',
        'message': ''
    }

    m = Message(
                user = request.user
            )
    if request.POST.get('message'):
        m.message = request.POST.get('message')
        m.save()
    
    if request.FILES.get('file'):
        content = request.FILES.get('file')
        upload_to = Message._meta.get_field_by_name("file")[0].upload_to

        filename = content.name
        path_name = os.path.normpath(force_text(datetime.datetime.now().strftime(force_str(upload_to))))
        file_name = os.path.normpath(default_storage.get_valid_name(os.path.basename(filename)))
        path_file = os.path.join(path_name, file_name)

        m.file = default_storage.save(path_file, content)
        m.save()


    return HttpResponse(json.dumps(context), content_type='application/json')


@require_POST
def loadMessages(request, *args, **kwargs):
    context = {
        'status': 'ok',
        'messages': []
    }

    items = Message.objects.all().values('mt_date', 'message', 'file', 'user__username').order_by('-mt_date')[:50]
    for item in items:
        file = None
        if item['file']:
            file = {
                "url": "%s%s" % (settings.MEDIA_URL, item['file']),
                "name": os.path.basename(item['file'])
            }
        context['messages'].append({
                'date': item['mt_date'].strftime("%d.%m.%Y %H:%M:%S"),
                'username': item['user__username'],
                'message': item['message'],
                'file': file
            })


    return HttpResponse(json.dumps(context), content_type='application/json')