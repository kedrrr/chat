# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from datetime import datetime

class Message(models.Model):
    mt_date = models.DateTimeField(default=datetime.now, blank=True, verbose_name = 'Дата и время')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=False, verbose_name = 'Пользователь')
    message = models.TextField(null=True, blank=True, verbose_name = 'Сообщение')
    file = models.FileField(upload_to = 'files/message', null=True, blank=True, verbose_name = 'Файл')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return ('%s %s: %s' % (self.mt_date, self.user.username, self.message or self.file)).encode('utf-8', errors='replace')
